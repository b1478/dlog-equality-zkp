# ZKP of discreet-logarithm equality

The goal of the repository is to have an easy to read code to understand how the zero-knowledge proof of discreet log equality  work. This is used in the atomic swap between XMR and BTC.

!!!!THE CODE IS NOT PRODUCTION READY!!!!

## Usage

```
python3 prove.py
python3 verify.py
```

## Files

- `xmr_zk_lib.py`: library of function used by the `prove.py` and `verify.py`
- `function-test.py`: test of the function defined in `xmr_zk_lib.py`
- `proof.txt`: here the proof created in `prove.py`
- `prove.py`: script to create the proof
- `verify.py`: script to verify the proof
- `galois_field.py`: the whole proof creation an verification but in the galois field: better to understand what's happening
- `MRL-010.pdf`: the monero research technical note on the discreet logarith equality zk proof
