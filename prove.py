import random
import time
import xmr_zk_lib as xzl


# general parameters
p=xzl.l
q=xzl.n
min_pq=min(p,q)
nbits=min_pq.bit_length()

def invMod(x,pp):
    return pow(x,-1,pp)

def gen_blinders(n,mod):
    r=[0]*n
    sum=0
    for i in range(n-1):
        r[i]=random.randrange(1,mod)
        sum+=(2**i*r[i]) %mod
    sump= (sum %mod)
    #r[n-1]=(mod-(2**(n-1)*sump)) %mod
    r[n-1]=(invMod(2**(n-1),mod)*(mod-sump)) %mod
    assert((sump+2**(n-1)*r[n-1]) %mod == 0)
    return r

def pad(x,nbits):
    l=x.bit_length()
    assert(l<=nbits)
    return '0'*(nbits-l)+bin(x)[2:]

def vector_from_pad(x):
    l=len(x)
    r=[0]*l
    for i in range(len(x)):
        r[i]=int(x[i])
    return r

def pedersen_comm(b,r,chain):
    l=len(b)
    assert(nbits == l)
    binary=''
    for i in b:
        binary+=str(i)
    x=int(binary,2)
    b=b[::-1] # reverse the b list; needed because we need little endian
    if chain == 'xmr':
        prime=xzl.q
        order=xzl.l
        HH=xzl.G
        HHprime=xzl.Gprime
    if chain == 'btc':
        prime=xzl.p
        order=xzl.n
        HH=xzl.H    
        HHprime=xzl.Hprime
    C=[HH]*l    
    D=[HH]*l
    sumPoint=None
    sumPoint2=None
    summ=0
    for i in range(l):
        summ+=(2**i)*r[i] %order
    assert(0==summ%order)
    for i in range(l):
        
        P1= xzl.point_mul(HHprime, b[i],chain)
        P2 = xzl.point_mul(HH, r[i]%order,chain)
        C[i]=xzl.point_add(P1, P2,chain)
        sumPoint=xzl.point_add(sumPoint, 
                        xzl.point_mul(C[i], pow(2,i,prime)%order, chain),
                        chain)
        print("chain=",chain," l=",l," i=",i," At ",round(100*i/(l-1),2),"%",end='\r')
        
    xHHprime=xzl.point_mul(HHprime, x%order, chain)
    assert(sumPoint==xHHprime)
    return C

def ring_sig(b,r,s,CG,CH):
    b=b[::-1] # reverse the b list; needed because we need little endian

    ll=len(b)
    for i in b:
        assert(i == 0 or i == 1)

    # print("check if r are really blinders in xmr")
    # summXMR=0
    # for i in range(ll):
    #     summXMR+=(2**i)*r[i] %xzl.l
    # assert(0==summXMR%xzl.l)

    # #check if s are really blinders in btc
    # print("check if s are really blinders in btc")
    # summBTC=0
    # for i in range(ll):
    #     summBTC+=(2**i)*s[i] %xzl.n
    # assert(0==summBTC%xzl.n)

    # print("check if  CG are Pedersen Commitments in xmr")
    # #("check if  CG are Pedersen Commitments in xmr")
    # sumPointXMR=None
    # for i in range(ll):
    #     sumPointXMR=xzl.point_add(sumPointXMR, 
    #                     xzl.point_mul(CG[i], pow(2,i,xzl.q)%xzl.l, 'xmr'),
    #                     'xmr')
    # xHHprime=xzl.point_mul(xzl.Gprime, x%xzl.l, 'xmr')
    # assert(sumPointXMR==xHHprime)
    
    # #check if  CH are Pedersen Commitments in btc
    # print("check if  CH are Pedersen Commitments in btc")
    # sumPointBTC=None
    # for i in range(ll):
    #     sumPointBTC=xzl.point_add(sumPointBTC, 
    #                     xzl.point_mul(CH[i], pow(2,i,xzl.p)%xzl.n, 'btc'),
    #                     'btc')
    # xHHprime=xzl.point_mul(xzl.Hprime, x%xzl.n, 'btc')
    # assert(sumPointBTC==xHHprime)
    
    
    eG0=[0]*ll
    eG1=[0]*ll
    eH0=[0]*ll
    eH1=[0]*ll
    
    a0=[0]*ll
    a1=[0]*ll

    b0=[0]*ll
    b1=[0]*ll
    
    count= 0
    GGG0=[0]*ll
    HHH0=[0]*ll
    GGG1=[0]*ll
    HHH1=[0]*ll
    for i in range(ll):
        

        count+=1
        print("current: ", round(100*count/ll,2),"%",end='\r')
        if b[i] == 0:
            j = random.randrange(1,xzl.l)
            k = random.randrange(1,xzl.n)
            GGG1[i] = xzl.bytes_from_point(xzl.point_mul(xzl.G, j, 'xmr'))
            HHH1[i] = xzl.bytes_from_point(xzl.point_mul(xzl.H, k, 'btc'))

            eG1[i] = xzl.hash_func(xzl.bytes_from_point(CG[i])
                                + xzl.bytes_from_point(CH[i])
                                + GGG1[i]
                                + HHH1[i],
                                'xmr')
            eH1[i] = xzl.hash_func(xzl.bytes_from_point(CG[i])
                                + xzl.bytes_from_point(CH[i])
                                + GGG1[i]
                                + HHH1[i],
                                'btc')
            
            a0[i]=random.randrange(1,xzl.l)
            b0[i]=random.randrange(1,xzl.n)
            # a0,iG - eG1,i(CiG - Gprime)
            # a0,iG - eG1,i(CiG - Gprime)
            GGG0[i] = xzl.bytes_from_point(
                xzl.point_add(
                    xzl.point_mul(xzl.G, a0[i], 'xmr'),
                    xzl.point_mul(
                        xzl.point_add(
                                CG[i],
                                xzl.point_mul(xzl.Gprime,-1, 'xmr'),
                                'xmr'), 
                        -eG1[i], 'xmr'),
                    'xmr')
                )

            HHH0[i] = xzl.bytes_from_point(
                xzl.point_add(
                    xzl.point_mul(xzl.H, b0[i], 'btc'),
                    xzl.point_mul(
                        xzl.point_add(
                                CH[i],
                                xzl.point_mul(xzl.Hprime,-1, 'btc'),
                                'btc'), 
                        -eH1[i], 'btc'),
                    'btc')
                )

            eG0[i]=xzl.hash_func(xzl.bytes_from_point(CG[i])
                                + xzl.bytes_from_point(CH[i])
                                + GGG0[i]
                                + HHH0[i],
                                'xmr')
            eH0[i]=xzl.hash_func(xzl.bytes_from_point(CG[i])
                                + xzl.bytes_from_point(CH[i])
                                + GGG0[i]
                                + HHH0[i],
                                'btc')

            a1[i] = (j + eG0[i]*r[i]) % xzl.l
            b1[i] = (k + eH0[i]*s[i]) % xzl.n
            
        if b[i] == 1:
            j = random.randrange(1,xzl.l)
            k = random.randrange(1,xzl.n)
            GGG0[i] = xzl.bytes_from_point(xzl.point_mul(xzl.G, j, 'xmr'))
            HHH0[i] = xzl.bytes_from_point(xzl.point_mul(xzl.H, k, 'btc'))

            eG0[i] = xzl.hash_func(xzl.bytes_from_point(CG[i])
                                + xzl.bytes_from_point(CH[i])
                                + GGG0[i]
                                + HHH0[i],
                                'xmr')
            eH0[i] = xzl.hash_func(xzl.bytes_from_point(CG[i])
                                + xzl.bytes_from_point(CH[i])
                                + GGG0[i]
                                + HHH0[i],
                                'btc')
            
            a1[i]=random.randrange(1,xzl.l)
            b1[i]=random.randrange(1,xzl.n)
            # a0,iG - eG1,i(CiG - Gprime)
            GGG1[i] = xzl.bytes_from_point(
                xzl.point_add(
                    xzl.point_mul(xzl.G, a1[i], 'xmr'),
                    xzl.point_mul(CG[i],-eG0[i]%xzl.l, 'xmr'),
                    'xmr'),
            )

            # b0,iH - eH1,i(CiH - H0)
            HHH1[i] = xzl.bytes_from_point(
                xzl.point_add(
                    xzl.point_mul(xzl.H, b1[i], 'btc'),
                    xzl.point_mul(CH[i],-eH0[i]%xzl.n, 'btc'),
                    'btc'),
                )
            
            
            eG1[i]=xzl.hash_func(xzl.bytes_from_point(CG[i])
                                + xzl.bytes_from_point(CH[i])
                                + GGG1[i]
                                + HHH1[i],
                                'xmr')
            eH1[i]=xzl.hash_func(xzl.bytes_from_point(CG[i])
                                + xzl.bytes_from_point(CH[i])
                                + GGG1[i]
                                + HHH1[i],
                                'btc')

            a0[i] = (j + eG1[i]*r[i]) % xzl.l
            b0[i] = (k + eH1[i]*s[i]) % xzl.n

    # return (eG0,eH0,a0,a1,b0,b1,eG1,eH1,GGG0,HHH0,GGG1,HHH1)
    return (eG0,eH0,a0,a1,b0,b1,eG1,eH1)
    
def create_proof(num):
    # p=xzl.l
    # q=xzl.n
    # min_pq=min(p,q)
    nbits=min_pq.bit_length()
    num = num % min_pq
    # x=random.randrange(1,min_pq)
    x=pad(num,nbits)
    b=vector_from_pad(x)
    print("Generating Blinders")
    r=gen_blinders(nbits, p)
    s=gen_blinders(nbits, q)

    print("\npedersen commitment for btc")
    CH=pedersen_comm(b, s, 'btc')
    
    print("\npedersen commitment for xmr")
    CG=pedersen_comm(b, r, 'xmr')

    print("\ndoing all the ring signatures")
    signature = ring_sig(b,r,s,CG,CH)
    proof = [xzl.point_mul(xzl.Gprime, num, 'xmr'),
             xzl.point_mul(xzl.Hprime, num, 'btc'),
             CG,
             CH]
    for i in signature:
        proof.append(i)
    
    return proof

x=random.randrange(1,min_pq)
# x=967823093624932668729449770675193900614604844897145333481636480252313784372
print("x is:",x)

start_time=time.time()
proof=create_proof(x)
end_time=time.time()

print("\nProof created in",round(end_time-start_time,1),"seconds")
with open('proof.txt','w') as f:
    f.write(str(proof))
    # f.write("\n"+str(proof))
# f.close()
