import random
import time
import xmr_zk_lib as xzl


# general parameters
p=xzl.l
q=xzl.n
min_pq=min(p,q)
min_pq=min(p,q)
nbits=min_pq.bit_length()

def invMod(x,p):
    return pow(x,-1,p)

def gen_blinders(n,mod):
    r=[0]*n
    sum=0
    for i in range(n-1):
        r[i]=random.randrange(1,mod)
        sum+=(2**i*r[i]) %mod
    sump= (sum %p)
    r[n-1]=(invMod(2**(n-1),mod)*(mod-sump)) %mod
    assert((mod-sump+2**(n-1)*r[n-1]) %mod)
    return r

def pad(x,nbits):
    l=x.bit_length()
    assert(l<=nbits)
    return '0'*(nbits-l)+bin(x)[2:]

def vector_from_pad(x):
    l=len(x)
    r=[0]*l
    for i in range(len(x)):
        r[i]=int(x[i])
    return r

def check_pedersen_comm(C,xHHprime,chain):
    l=len(C)
    if chain == 'xmr':
        prime=xzl.q
        order=xzl.l
        HH=xzl.G
        HHprime=xzl.Gprime
    if chain == 'btc':
        prime=xzl.p
        order=xzl.n
        HH=xzl.H    
        HHprime=xzl.Hprime
    sumPoint=C[0]
    for i in range(1,l):
        # P1 = xzl.point_mul(HHprime, b[i]%order,chain)
        # P2 = xzl.point_mul(HH, r[i],chain)
        # C[i]=xzl.point_add(P1, P2,chain)
        sumPoint=xzl.point_add(sumPoint, 
                            xzl.point_mul(C[i], pow(2,i,prime)%order, chain),
                            chain)
        # print(sumPoint)
        print("chain=",chain," l=",l," i=",i," At ",round(100*i/l,2),"%",end='\r')
    # print(xHHprime)
    print()
    if sumPoint==xHHprime:
        return True
    return False

# def ring_sig(b,r,s,CG,CH):
#     for i in b:
#         assert(i == 0 or i == 1)
    
#     ll=len(b)
    
#     eG0=[0]*ll
#     eG1=[0]*ll
#     eH0=[0]*ll
#     eH1=[0]*ll
    
#     a0=[0]*ll
#     a1=[0]*ll

#     b0=[0]*ll
#     b1=[0]*ll
    
#     count= 0
#     for i in range(ll):
#         count+=1
#         print("current: ", round(100*count/ll,2),"%",end='\r')
#         if b[i] == 0:
#             j = random.randrange(1,xzl.l)
#             k = random.randrange(1,xzl.n)
#             eG1[i] = xzl.hash_func(xzl.bytes_from_point(CG[i])
#                                 + xzl.bytes_from_point(CH[i])
#                                 + xzl.bytes_from_point(xzl.point_mul(xzl.G, j, 'xmr'))
#                                 + xzl.bytes_from_point(xzl.point_mul(xzl.H, k, 'btc')),
#                                 'xmr')
#             eH1[i] = xzl.hash_func(xzl.bytes_from_point(CG[i])
#                                 + xzl.bytes_from_point(CH[i])
#                                 + xzl.bytes_from_point(xzl.point_mul(xzl.G, j, 'xmr'))
#                                 + xzl.bytes_from_point(xzl.point_mul(xzl.H, k, 'btc')),
#                                 'xmr')
            
#             a0[i]=random.randrange(1,xzl.l)
#             b0[i]=random.randrange(1,xzl.n)
#             # a0,iG - eG1,i(CiG - Gprime)
#             # a0,iG - eG1,i(CiG - Gprime)
#             GGG = xzl.bytes_from_point(
#                 xzl.point_add(
#                     xzl.point_mul(xzl.G, a0[i], 'xmr'),
#                     xzl.point_opposite(
#                         xzl.point_mul(
#                             xzl.point_add(
#                                 CG[i],
#                                 xzl.point_opposite(xzl.Gprime, 'xmr'),
#                                 'xmr'
#                             ), 
#                             eG1[i], 'xmr'),'xmr'),
#                             'xmr'
#                 )
#             )

#             # b0,iH - eH1,i(CiH - H0)
#             HHH = xzl.bytes_from_point(
#                 xzl.point_add(
#                     xzl.point_mul(xzl.H, b0[i], 'btc'),
#                     xzl.point_opposite(
#                         xzl.point_mul(
#                             xzl.point_add(
#                                 CH[i],
#                                 xzl.point_opposite(xzl.Hprime, 'btc'),
#                                 'btc'
#                             ), 
#                             eH1[i], 'btc'), 'btc'),
#                             'btc'
#                 )
#             )


#             eG0[i]=xzl.hash_func(xzl.bytes_from_point(CG[i])
#                                 + xzl.bytes_from_point(CH[i])
#                                 + GGG
#                                 + HHH,
#                                 'xmr')
#             eG0[i]=xzl.hash_func(xzl.bytes_from_point(CG[i])
#                                 + xzl.bytes_from_point(CH[i])
#                                 + GGG
#                                 + HHH,
#                                 'btc')

#             a1[i] = (j + eG0[i]*r[i]) % xzl.l
#             b1[i] = (k + eH0[i]*s[i]) % xzl.n
#         if b[i] == 1:
#             j = random.randrange(1,xzl.l)
#             k = random.randrange(1,xzl.n)
#             eG0[i] = xzl.hash_func(xzl.bytes_from_point(CG[i])
#                                 + xzl.bytes_from_point(CH[i])
#                                 + xzl.bytes_from_point(xzl.point_mul(xzl.G, j, 'xmr'))
#                                 + xzl.bytes_from_point(xzl.point_mul(xzl.H, k, 'btc')),
#                                 'xmr')
#             eH0[i] = xzl.hash_func(xzl.bytes_from_point(CG[i])
#                                 + xzl.bytes_from_point(CH[i])
#                                 + xzl.bytes_from_point(xzl.point_mul(xzl.G, j, 'xmr'))
#                                 + xzl.bytes_from_point(xzl.point_mul(xzl.H, k, 'btc')),
#                                 'xmr')
            
#             a1[i]=random.randrange(1,xzl.l)
#             b1[i]=random.randrange(1,xzl.n)
#             # a0,iG - eG1,i(CiG - Gprime)
#             GGG = xzl.bytes_from_point(
#                 xzl.point_add(
#                     xzl.point_mul(xzl.G, a1[i], 'xmr'),
#                     xzl.point_opposite(
#                         xzl.point_mul(CG[i],eG0[i], 'xmr'),
#                         'xmr'),
#                         'xmr'
#                 )
#             )

#             # b0,iH - eH1,i(CiH - H0)
#             HHH = xzl.bytes_from_point(
#                 xzl.point_add(
#                     xzl.point_mul(xzl.H, b1[i], 'btc'),
#                     xzl.point_opposite(
#                         xzl.point_mul(CH[i],eH0[i], 'btc'),
#                         'btc'),
#                         'xmr'
#                 )
#             )

#             eG0[i]=xzl.hash_func(xzl.bytes_from_point(CG[i])
#                                 + xzl.bytes_from_point(CH[i])
#                                 + GGG
#                                 + HHH,
#                                 'xmr')
#             eG0[i]=xzl.hash_func(xzl.bytes_from_point(CG[i])
#                                 + xzl.bytes_from_point(CH[i])
#                                 + GGG
#                                 + HHH,
#                                 'btc')

#             a0[i] = (j + eG1[i]*r[i]) % xzl.l
#             b0[i] = (k + eH1[i]*s[i]) % xzl.n
#     return (eG0,eH0,a0,a1,b0,b1)
    
def create_proof(num):
    # p=xzl.l
    # q=xzl.n
    # min_pq=min(p,q)
    nbits=min_pq.bit_length()
    num = num % min_pq
    # x=random.randrange(1,min_pq)
    x=pad(num,nbits)
    b=vector_from_pad(x)
    print("Generating Blinders")
    r=gen_blinders(nbits, p)
    s=gen_blinders(nbits, q)

    print("\npedersen commitment for xmr")
    CG=pedersen_comm(b, r, 'xmr')
    print("\npedersen commitment for btc")
    CH=pedersen_comm(b, s, 'btc')

    print("\ndoing all the ring signatures")
    signature = ring_sig(b,r,s,CG,CH)
    proof = [xzl.point_mul(xzl.Gprime, num, 'xmr'),
             xzl.point_mul(xzl.Hprime, num, 'btc'),
             CG,
             CH]
    for i in signature:
        proof.append(i)
    
    return proof

def validateProof(proof):

    xGprime=proof[0]
    xHprime=proof[1]
    CG=proof[2]
    CH=proof[3]
    eG0=proof[4]
    eH0=proof[5]
    a0=proof[6]
    a1=proof[7]
    b0=proof[8]
    b1=proof[9]
    

    eG0prime=[0]*nbits
    eH0prime=[0]*nbits
    eG1=[0]*nbits
    eH1=[0]*nbits


    if check_pedersen_comm(CG, xGprime, 'xmr') and check_pedersen_comm(CH, xHprime, 'btc'):

        GGG0=[0]*nbits
        HHH0=[0]*nbits
        GGG1=[0]*nbits
        HHH1=[0]*nbits
        for i in range(nbits):
            print("checking hashes at ",round(100*i/(nbits-1),2),"%",end='\r')
            # a0,iG - eG1,i(CiG - Gprime)
            GGG1[i] = xzl.bytes_from_point(
                xzl.point_add(
                    xzl.point_mul(xzl.G, a1[i], 'xmr'),
                    xzl.point_mul(CG[i],-eG0[i]%xzl.l, 'xmr'),
                    'xmr'),
                )

            # b0,iH - eH1,i(CiH - H0)
            HHH1[i] = xzl.bytes_from_point(
                xzl.point_add(
                    xzl.point_mul(xzl.H, b1[i], 'btc'),
                    xzl.point_mul(CH[i],-eH0[i]%xzl.n, 'btc'),
                    'btc'),
                )

            eG1[i]=xzl.hash_func(xzl.bytes_from_point(CG[i])
                                + xzl.bytes_from_point(CH[i])
                                + GGG1[i]
                                + HHH1[i],
                                'xmr')
            eH1[i]=xzl.hash_func(xzl.bytes_from_point(CG[i])
                                + xzl.bytes_from_point(CH[i])
                                + GGG1[i]
                                + HHH1[i],
                                'btc')
            
            GGG0[i] = xzl.bytes_from_point(
                xzl.point_add(
                    xzl.point_mul(xzl.G, a0[i], 'xmr'),
                        xzl.point_mul(
                            xzl.point_add(
                                CG[i],
                                xzl.point_mul(xzl.Gprime,-1, 'xmr'),
                                'xmr'
                                ), 
                            -eG1[i], 'xmr')
                ,'xmr')
            )

            # b0,iH - eH1,i(CiH - H0)
            HHH0[i] = xzl.bytes_from_point(
                xzl.point_add(
                    xzl.point_mul(xzl.H, b0[i], 'btc'),
                        xzl.point_mul(
                            xzl.point_add(
                                CH[i],
                                xzl.point_mul(xzl.Hprime,-1, 'btc'),
                                'btc'
                            ), 
                            -eH1[i], 'btc'),
                            'btc'
                )
            )

            eG0prime[i]=xzl.hash_func(xzl.bytes_from_point(CG[i])
                                + xzl.bytes_from_point(CH[i])
                                + GGG0[i]
                                + HHH0[i],
                                'xmr')
            eH0prime[i]=xzl.hash_func(xzl.bytes_from_point(CG[i])
                                + xzl.bytes_from_point(CH[i])
                                + GGG0[i]
                                + HHH0[i],
                                'btc')
            if eG0prime[i] != eG0[i] or eH0prime[i] != eH0[i]:
                return False
        if eG0prime == eG0 and eH0prime == eH0:
            return True
    else:
        return False

proof=[]
with open('proof.txt','r') as f:
    proof=eval(f.read())

start_time=time.time()
ver=validateProof(proof)
end_time=time.time()
print()
xGprime=proof[0]
xHprime=proof[1]
if ver == True:
    print("points\n",xGprime,"\n and\n",xHprime,"\nhave the same discreet logarithm. Verified in time:",round(end_time-start_time,2),"seconds")
else:
    print("points\n",xGprime,"\n and\n",xHprime,"DON'T have the same discreet logarithm. Verified in time:",round(end_time-start_time,2),"seconds")


