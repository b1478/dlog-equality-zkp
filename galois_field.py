import random 

p=min_pq=101
nbits=p.bit_length()

def addMod(a,b,mod):
    return (a+b)%p

def mulMod(A,b,mod):
    return (A*b)%p

def invMod(x,p):
    return pow(x,-1,p)
def pad(x,nbits):
    l=x.bit_length()
    assert(l<=nbits)
    return '0'*(nbits-l)+bin(x)[2:]

def vector_from_pad(x):
    l=len(x)
    r=[0]*l
    for i in range(len(x)):
        r[i]=int(x[i])
    return r

def is_generator(x,mod):
    phi=mod-1
    print("phi is\t",phi)
    vect=[0]*phi
    giusto=[0]*phi
    # vect=[]
    # giusto=[]
    for i in range(phi):
        print("i is\t",i)
        giusto[i]=i+1
        vect[i]=pow(x,i,mod)
    vect.sort()
    if vect == giusto:
        return True
    else:    
        return False

def get_generator(mod):
    isg=False
    x=1
    while not isg:
        x+=1
        isg=is_generator(x, mod)
        
    print("the generator is",x)
    return x


def gen_blinders(n,mod):
    r=[0]*n
    sum=0
    for i in range(n-1):
        r[i]=random.randrange(1,mod)
        sum+=(2**i*r[i]) %mod
    sump= (sum %p)
    r[n-1]=(invMod(2**(n-1),mod)*(mod-sump)) %mod
    assert((mod-sump-2**(n-1)*r[n-1]) %mod == 0)
    return r

def pedersen_comm(b,r,chain=None):
    l=len(b)
    binary=''
    for i in b:
        binary+=str(i)
    x=int(binary,2)
    b=b[::-1] # reverse the string; needed because we need little endian
    C=[0]*l
    HH=gen
    HHprime=other_num
    sumNum=0
    for i in range(l):
        P1 = mulMod(HHprime,b[i],p)
        P2 = mulMod(HH,r[i],p)
        C[i]=addMod(P1,P2,p)
        sumNum=addMod(sumNum,
                        mulMod(C[i],2**i%p,p),
                        p)
        print("chain=",chain," l=",l," i=",i," At ",round(100*i/(l-1),2),"%",end='\r')
    assert (sumNum %p) == ((HHprime*x) %p)
    return C

for other_num in range(2,p):
    print()
    print("other_num is\t",other_num)
    num=random.randrange(1,min_pq)
    print("x is:",num)
    gen=1
    #other_num=(7*gen) %p
    x=pad(num,nbits)
    b=vector_from_pad(x)
    print("Generating Blinders")
    r=gen_blinders(nbits, p)
    print("Generating Commits")
    pedersen_comm(b, r)
