from typing import Tuple, Optional
import hashlib

# https://ed25519.cr.yp.to/python/ed25519.py
# https://github.com/warner/python-pure25519/blob/master/pure25519/basic.py


def invMod(x,p):
    return pow(x,-1,p)

def xrecover(y):
  xx = (y*y-1) * invMod(d*y*y+1,q)
  x = pow(xx,(q+3)//8,q)
  if (x*x - xx) % q != 0: x = (x*I) % q
  if x % 2 != 0: x = q-x
  return x

# Bitcoin Elliptic curve parameters
p = 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFC2F
n = 2**256-432420386565659656852420866394968145599
H = (0x79BE667EF9DCBBAC55A06295CE870B07029BFCDB2DCE28D959F2815B16F81798,
     0x483ADA7726A3C4655DA4FBFC0E1108A8FD17B448A68554199C47D08FFB10D4B8)
     

# Monero Elliptic curve parameters
q = 2**255-19
d = ((-121665%q) * invMod(121666,q)) %q
l = 2**252 + 27742317777372353535851937790883648493

I = pow(2,(q-1)//4,q)
Gy = 4 * invMod(5,q)
Gx = xrecover(Gy)
G = (Gx % q,Gy % q)

# Points are tuples of X and Y coordinates
# the point at infinity is represented by the None keyword
Point = Tuple[int, int]

# Points are tuples of X and Y coordinates
# the point at infinity is represented by the None keyword
Point = Tuple[int, int]

# Get bytes from an int
def bytes_from_int(x: int) -> bytes:
    return x.to_bytes(32, byteorder="big")

# Get bytes from a point
def bytes_from_point(P: Point) -> bytes:
    return bytes_from_int(x(P))

# Get an int from bytes
def int_from_bytes(b: bytes) -> int:
    return int.from_bytes(b, byteorder="big")

def hash_func(m,chain:str):
    if chain!='xmr' and chain !='btc':
        raise ValueError(
            'Chain should be xmr or btc for hash_func')
    if chain=='xmr':
        hash_digest=hashlib.sha512(m).digest()
        return int_from_bytes(hash_digest) % l
    if chain=='btc':
        hash_digest= hashlib.sha256((hashlib.sha256(m).digest())).digest()
        return int_from_bytes(hash_digest) % n

# Get x coordinate from a point
def x(P: Point) -> int:
    return P[0]

# Get y coordinate from a point
def y(P: Point) -> int:
    return P[1]

def isoncurve(P,chain):
    if chain!='xmr' and chain !='btc':
        raise ValueError(
            'Chain should be xmr or btc for isoncurve')
    if chain == 'btc':
        return True
    if chain == 'xmr':
        x = P[0]
        y = P[1]
        return ((-x*x + y*y - 1 - d*x*x*y*y) % q == 0)

# Point addition
def point_add(P1: Optional[Point], P2: Optional[Point],chain: str) -> Optional[Point]:
    if chain!='xmr' and chain !='btc':
        raise ValueError(
            'Chain should be xmr or btc for point_add')
    if P1 is None:
        return P2
    if P2 is None:
        return P1
    if chain == 'btc':
        if (x(P1) == x(P2)) and (y(P1) != y(P2)):
            return None
        if P1 == P2:
            lam = (3 * x(P1) * x(P1) * pow(2 * y(P1), p - 2, p)) % p
        else:
            lam = ((y(P2) - y(P1)) * pow(x(P2) - x(P1), p - 2, p)) % p
        x3 = (lam * lam - x(P1) - x(P2)) % p
        return (x3, (lam * (x(P1) - x3) - y(P1)) % p)
    if chain == 'xmr':
        x1 = x(P1)
        y1 = y(P1)
        x2 = x(P2)
        y2 = y(P2)
        x3 = (x1*y2+x2*y1) * invMod(1+d*x1*x2*y1*y2,q)
        y3 = (y1*y2+x1*x2) * invMod(1-d*x1*x2*y1*y2,q)
        ppp=(x3 % q,y3 % q)
        if not isoncurve(ppp, chain): print("ppp is", ppp)
#        assert(isoncurve(ppp, chain))
        if (x3 % q == 0 and y3 % q == 1):
            return None
        else:
            return ppp



# Point multiplication
def point_mul(P: Optional[Point], num_mul: int, chain: str) -> Optional[Point]:
    if chain!='xmr' and chain !='btc':
        raise ValueError(
            'Chain should be xmr or btc for point_mul')
    R = None
    if num_mul==0: return R
    if chain == 'btc':
        nbits=n.bit_length()
        num_mul=num_mul%n #module base point order
    if chain == 'xmr':
        nbits=l.bit_length()
        num_mul=num_mul%l #module base point order
    # print("chain is",chain,"nbits is",nbits,"in point mul")
    for i in range(nbits):
        #print("i in point_mul is\t",i," n in point_mul is\t ",n,end='\r')
        if (num_mul >> i) & 1:
            R = point_add(R, P,chain)
        P = point_add(P, P,chain)
    return R

def point_opposite(P,chain):
    if chain!='xmr' and chain !='btc':
        raise ValueError(
            'Chain should be xmr or btc for point_mul')
    if chain == 'btc':
        mod=p
        return (x(P),mod-y(P))
    if chain == 'xmr':
        return point_mul(P,-1,chain)

# alt non working version
def point_mul_alt(P: Optional[Point], n: int, chain: str) -> Optional[Point]:
    if chain!='xmr' and chain !='btc':
        raise ValueError(
            'Chain should be xmr or btc for point_mul')
    R = None
    if n==0: return R
    if chain == 'btc':
        nbits=n.bit_length()
        for i in range(nbits):
        #print("i in point_mul is\t",i," n in point_mul is\t ",n,end='\r')
            if (n >> i) & 1:
                R = point_add(R, P,chain)
            P = point_add(P, P,chain)
        return R
    if chain == 'xmr':
        Q = point_mul(P,n//2,chain)
        Q = point_add(Q,Q,chain)
        if n & 1: Q = point_add(Q,P,chain)
        return Q


    # Generate public key from an int
def pubkey_gen_from_int(seckey: int) -> bytes:
    if chain!='xmr' and chain !='btc':
        raise ValueError(
            'Chain should be xmr or btc for pubkey_gen_from_int')
    if chain=='xmr':
        P = point_mul(G, seckey)
    if chain=='btc':
        P = point_mul(H, seckey)
    assert P is not None
    return bytes_from_point(P)

# Generate public key from a hex
def pubkey_gen_from_hex(seckey: hex) -> bytes:
    seckey = bytes.fromhex(seckey)
    d0 = int_from_bytes(seckey)
    if not (1 <= d0 <= n - 1):
        raise ValueError(
            'The secret key must be an integer in the range 1..n-1.')
    P = point_mul(G, d0)
    assert P is not None
    return bytes_from_point(P)


# Get a point from bytes on the BTC curve secp256k1
# Get a point from bytes on the XMR curve curve25519
def lift_x_square_y(b: bytes, chain) -> Optional[Point]:
    if chain!='xmr' and chain !='btc':
        raise ValueError(
            'Chain should be xmr or btc for lift_x_square_y')
    if chain=='xmr':
        x = (int_from_bytes(b) +1)% q
        y_sq = (((pow(x, 2, q) + 1) % p) * invMod(1-d*pow(x, 2, q), q)) % q
        y = pow(y_sq, (q + 3) // 8, q)
        y2 = pow(y_sq, (q + 1) // 4, q)
        print("y is", y)
        print("y2 is", y2)

        print("pow(y, 2, q) is \t",pow(y, 2, q))
        print("y_sq is \t",y_sq)
        if pow(y, 2, q) != y_sq:
            return None
    if chain=='btc':
        x = int_from_bytes(b) % p
        y_sq = (pow(x, 3, p) + 7) % p
        y = pow(y_sq, (p + 1) // 4, p)
        if pow(y, 2, p) != y_sq:
            return None
    return (x, y)

def new_curve_generator(b: bytes, chain) -> Optional[Point]:
    if chain!='xmr' and chain !='btc':
        raise ValueError(
            'Chain should be xmr or btc for lift_x_square_y')
    if chain=='xmr':
        on_curve = False
        i=0
        while not on_curve:
            print("i= ",i)
            x = (int_from_bytes(b)+i)% q
            y_sq = (((pow(x, 2, q) + 1) % q) * invMod(1-d*pow(x, 2, q), q)) % q
            # (q + 3) // 8 because q % 8 ==5 
            # https://www.wikiwand.com/en/Quadratic_residue#/Prime_or_prime_power_modulus
            y = pow(y_sq, (q + 3) // 8, q) 
        
            if pow(y, 2, q) != y_sq:
                # print("pow(y, 2, q) is \t",pow(y, 2, q))
                # print("y_sq is \t",y_sq)
                i=i+1     
                          
            else:
                on_curve=True
        return (x, y if y % 2 == 0 else q -y)
    if chain=='btc':
        on_curve = False
        i=0
        while not on_curve:
            print("i= ",i)
            i=i+1   
            x = (int_from_bytes(b) + i) % p
            y_sq = (pow(x, 3, p) + 7) % p
            # (p + 1) // 4 because p % 4 == 3 
            # https://www.wikiwand.com/en/Quadratic_residue#/Prime_or_prime_power_modulus
            y = pow(y_sq, (p + 1) // 4, p)
            if pow(y, 2, p) != y_sq:
                # print("pow(y, 2, q) is \t",pow(y, 2, q))
                # print("y_sq is \t",y_sq)
                pass               
            else:
                on_curve=True
        return (x, y if y % 2 == 0 else p -y)
    
# new generators
# https://crypto.stackexchange.com/questions/25581/second-generator-for-secp256k1-curve
# Hprime=new_curve_generator(hashlib.sha256(hashlib.sha256(bytes_from_point(H)).digest()).digest(),'btc')
Hprime=(20398558627212419837080846849256994710341861217385892020943523779934444519786, 94065833198860723780314493411634732718468938263404879459710259352786079254826)


# Gnocofactor=new_curve_generator(hashlib.sha512(bytes_from_point(G)).digest(),'xmr')
Gnocofactor=(36338171149784111376175438719232338180223784889520697244108369774565144412567, 36682346970005697250268611602270831852062941076840702904037424013740444353432)
# https://crypto.stackexchange.com/questions/58126/curve-point-generators-for-ecc?rq=1
# https://datatracker.ietf.org/doc/html/rfc7748#page-4
# Gprime=point_mul(Gnocofactor, 8, 'xmr')
Gprime=(29205630840488400693679088203160788562206509724175355492237842459076697341585, 45870240180351932530282716009833705683510813709525065215364434211225731306930)


