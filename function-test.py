
import xmr_zk_lib as xzl


# general parameters
p=xzl.l
q=xzl.n
min_pq=min(p,q)
nbits=min_pq.bit_length()

A=xzl.point_mul(xzl.Hprime, 7, 'btc')
print("does A+(-A)=O in BTC?",xzl.point_add(A,xzl.point_opposite(A, 'btc'),'btc')==None)
C=xzl.point_mul(xzl.Hprime, 3, 'btc')
print("does 7G+(-3G)=4G in BTC?",xzl.point_add(A,xzl.point_opposite(C, 'btc'),'btc')==xzl.point_mul(xzl.Hprime, 4, 'btc'))
print("does -7G+(3G)=-4G in BTC?",xzl.point_add(C,xzl.point_opposite(A, 'btc'),'btc')==xzl.point_mul(xzl.Hprime, -4, 'btc'))
print("does opposite(C) == mult(C,-1)in BTC?",xzl.point_opposite(A, 'btc')==xzl.point_mul(A, -1, 'btc'))
print("does 5C==(q+5)C in BTC?",xzl.point_mul(A, 5, 'btc')==xzl.point_mul(A, 5+q, 'btc'))
print("does 238156C==(q+238156)C in BTC?",xzl.point_mul(A, 238156, 'btc')==xzl.point_mul(A, 238156+q, 'btc'))

print()

B=xzl.point_mul(xzl.Gprime, 7, 'xmr')
print("does B+(-B)=O in XMR?",xzl.point_add(B,xzl.point_opposite(B, 'xmr'),'xmr')==None)
D=xzl.point_mul(xzl.Gprime, 3, 'xmr')
print("does 7G+(-3G)=4G in XMR?",xzl.point_add(B,xzl.point_opposite(D, 'xmr'),'xmr')==xzl.point_mul(xzl.Gprime, 4, 'xmr'))
print("does -7G+3G=-4G in XMR?",xzl.point_add(D,xzl.point_opposite(B, 'xmr'),'xmr')==xzl.point_mul(xzl.Gprime, -4, 'xmr'))
print("does opposite(C) == mult(C,-1)in XMR?",xzl.point_opposite(D, 'xmr')==xzl.point_mul(D, -1, 'xmr'))
print("does 5C==(q+5)C in BTC?",xzl.point_mul(D, 5, 'xmr')==xzl.point_mul(D, 5+p, 'xmr'))
print("does 238156C==(p+238156)C in BTC?",xzl.point_mul(D, 238156, 'xmr')==xzl.point_mul(D, 238156+p, 'xmr'))